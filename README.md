# ansible-fedora

This repository contains Ansible playbooks to setup **my** personal laptop with Fedora. **Use at your own risk.**

## How to use

### Requirements
- git
- make
- Python 3 (with python-pip module installed)

### Run
1. Update your user details and locale in `main.yml` and `hosts.ini`.
1. Download dependencies.
    ```
    $ make prepare
    ```
1. The very fist time after fresh device installation, update all installed packages by
    ```
    $ make play_init
    ```
1. See what changes will be done **before** they are performed.
    ```
    $ make check
    ```
1. Run the playbook.
    ```
    $ make play
    ```

### Re-build the development toolbox
1. Stop and remove the development toolbox.
    ```
    $ toolbox rm --force development
    ```
1. Run the playbook as described above.

## Contributing

This repository contains configuration for **my** personal laptop. You are welcome to browse it, get inspired
or reuse it for your own device. I am generally open to review any changes that improve the code but preserve
existing functionality. Any other changes are unlikely to be accepted.
